import 'package:flutter/material.dart';
import 'package:cartoons_flutter/model/character.dart';

class CharacterWidget extends StatelessWidget {
  final Character character;

  const CharacterWidget({Key? key, required this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //TODO 0: Usa widgets básicos que ya conoces para crear este widget/cell.
    // Algunos consejos útiles:
    // * No elimine el widget Container(), agregue todos sus widgets dentro. Recuerde que el widget Container
    //   tiene un solo hijo, pero su primer paso probablemente debería ser agregar un widget Row como ese hijo.
    // * Cargue imágenes con Image.asset (character.image, ...
    // * En el widget Container, siéntase libre de usar decoration:
    //   BoxDecoration (color: Colors.black12, borderRadius: BorderRadius.all (Radius.circular (20.0)))
    return Container(
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: const BoxDecoration(
              color: Color.fromARGB(255, 131, 101, 184),
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
            ),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset(character.image,height: 200,),
                  ],
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Column(
                  children: [
                     Text(character.name,style: 
                        const TextStyle(
                          fontSize: 29,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 20.0),
                        child: Row(children: [
                          Container(
                            margin: const EdgeInsets.only(right: 10.0),
                            width: 48.0,
                            height: 48.0,
                            decoration:  BoxDecoration(
                            color: (character.stars >= 5.0) ? Colors.green : Colors.orange,
                            borderRadius: const BorderRadius.all(Radius.circular(100)),
                            ),
                            child: Center(child: 
                               Text(character.stars.toString(),style: TextStyle(color: Colors.white),),
                            ),
                            
                          ),
                          Text(character.jobTitle)
                        ]),
                      )
                      
                      
                  ],
                ),
                )
                ],
            ),
          )),
    );
  }
}
